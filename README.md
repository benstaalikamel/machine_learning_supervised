# Supervised Machine Learning

In this repository, you will find all supervised algorithms that i practice.
This repository is organized as fellow:

- KNN_folder
  - KNN_course
  - KNN_exemple_folder
    - KNN_ReadMe
    - Dataset
    - KNN_Ipynb


- LinearRegression_folder
  - LinearRegression_course
  - LinearRegression_exemple_folder
    - LinearRegression_ReadMe 
    - Dataset
    - LinearRegression_Ipynb

- PolynomialRegression_folder
  - PolynomialRegression_course
  - PolynomialRegression_exemple_folder
    - PolynomialRegression_ReadMe 
    - Dataset
    - PolynomialRegression_Ipynb

- LogisticRegression_folder
  - LogisticRegression_course
  - LogisticRegression_exemple_folder
    - LogisticRegression_ReadMe 
    - Dataset
    - LogisticRegression_Ipynb

- SVM_folder
  - SVM_course
  - SVM_exemple_folder
    - SVM_ReadMe 
    - Dataset
    - SVM_Ipynb

- Artificial Neural Networks_folder 
  - ArtificialNeuralNetworks_course
  - ArtificialNeuralNetworks_exemple_folder
    - ArtificialNeuralNetworks_ReadMe 
    - Dataset
    - ArtificialNeuralNetworks_Ipynb

- Decision Trees_folder
  - DecisionTrees_course
  - DecisionTrees_exemple_folder
    - DecisionTrees_ReadMe 
    - Dataset
    - DecisionTrees_Ipynb

- Random Forests_folder
  - RandomForests_course
  - RandomForests_exemple_folder
    - RandomForests_ReadMe 
    - Dataset
    - RandomForests_Ipynb


(I started recently on completing this repository 😊 , the content will probably be edited for improvement)

Hope you enjoy it.👍
