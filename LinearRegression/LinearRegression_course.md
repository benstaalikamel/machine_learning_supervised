1 - Model function : 
    
    y = a x + b

2 - Cost function (h(θ)) :
    
    h(a,b) = (f(x^i) - y^i)^2
 
    
3- Optimization algorithm : 
    
    Gradient descent 

    (If we want to test with the normal equations method as optimization algorithm we can just use Sklearn.linear_model.LinearRegression)